---
title: "BTI7056-DB"
subtitle: "Homework 6"
author:
    - Marius Schär
    - Severin Kaderli
extra-info: true
institute: "Berner Fachhochschule"
department: "Technik und Informatik"
lecturer: "Dr. Kai Brünnler"
lang: "de-CH"
toc: false
...

# Aufgabe 1
> Es ist das umseitige Schema gegeben. Die zugehörige SQLite-Datenbank ist in
> `publications.db`. Geben Sie SQL-Queries für alle folgenden Aufgaben an. Testen
> Sie die Queries mit dieser Datenbank.

> 1. Finden Sie alle Autoren, die in San Francisco wohnen.
```{.sql .numberLines}
SELECT * FROM authors WHERE city = "San Fransisco";
```

> 2. Wieviele Titel beginnen mit "S"?
```{.sql .numberLines}
SELECT COUNT(*) FROM titles WHERE title LIKE "S%";
```

> 3. Bestimmen Sie den durchschnittlichen Preis eines Titels.
```{.sql .numberLines}
SELECT AVG(price) FROM titles;
```

> 4. Geben Sie das Datum aller Verkäufe des Ladens "Bookbeat" an.
```{.sql .numberLines}
SELECT date FROM sales WHERE stor_id IN (
    SELECT stor_id FROM stores WHERE stor_name = "Bookbeat"
);
```

> 5. Geben Sie alle Titel aus, die im Laden "Bookbeat" verkauft wurden.
```{.sql .numberLines}
SELECT DISTINCT ti.title sold
FROM titles ti
JOIN salesdetail sd ON ti.title_id = sd.title_id
JOIN stores st ON sd.stor_id = sd.stor_id
WHERE st.stor_name = 'Bookbeat';
```
   
# Aufgabe 2
> 1. Finden Sie Titel und Preis des teuersten Buches.
```{.sql .numberLines}
SELECT title, price FROM titles ORDER BY price DESC LIMIT 1;
```

> 2. Finden Sie sämtliche Bücher, die teurer als das billigste Psychologie-Buch
> sind.
```{.sql .numberLines}
SELECT * FROM titles WHERE price > (
    SELECT MIN(price) FROM titles WHERE type LIKE "psychology%"
);
```

> 3. Finden Sie die Autoren, die in einem Staat wohnen, in dem es keinen der
> erfassten Läden gibt.
```{.sql .numberLines}
SELECT * FROM authors WHERE state NOT IN (
    SELECT state FROM stores
);
```

> 4. Geben Sie die Städte an, in denen es sowohl Autoren wie auch Verleger
gibt.
```{.sql .numberLines}
SELECT city FROM publishers WHERE city IN (
    SELECT city FROM authors
);
```

# Aufgabe 3
> 1. Bestimmen Sie alle Bücher, die den gleichen Typ besitzen wie das Buch
> "Net Etiquette".
```{.sql .numberLines}
SELECT * FROM titles WHERE type IN (
    SELECT type FROM titles WHERE title = "Net Etiquette"
);
```

> 2. Geben Sie einen SQL Ausdruck an, der die Büchertypen zusammen mit
> der Anzahl Bücher jedes Typs ausgibt.
```{.sql .numberLines}
SELECT TYPE, count(1)
FROM titles
GROUP BY TYPE;
```

> 3. Geben Sie einen SQL Ausdruck an, der die Büchertypen zusammen mit der
> Anzahl Bücher jedes Typs ausgibt, von denen es mehr als 2 verschiedene
> Bücher gibt.
```{.sql .numberLines}
SELECT TYPE, count(1) amount
FROM titles
GROUP BY TYPE
HAVING amount > 2;
```

> 4. Geben Sie einen SQL Ausdruck an, der die Anzahl Autoren pro Staat
> auflistet, wobei die Ausgabe nach Anzahl Autoren sortiert sein soll.
```{.sql .numberLines}
SELECT state, count(1) amount
FROM authors
GROUP BY state
ORDER BY amount DESC;
```

\newpage
> 5. Bestimmen Sie alle Publisher, welche weniger Bücher herausgegeben haben
> als der Durchschnitt.
```{.sql .numberLines}
WITH published_amount AS (
  SELECT pub_name, count(title_id) AS amount
  FROM titles
  NATURAL JOIN publishers
  GROUP BY pub_name
)
SELECT *
FROM
  (SELECT avg(amount) AS average FROM published_amount),
  (SELECT pub_name, amount FROM published_amount)
WHERE amount < average
```
